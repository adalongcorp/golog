# Golog
A zero-dependency, incredibly naive JSON logger. It does nothing else than output JSON logs to stdout.

## Usage
```golang
golog.Infof("This is an interesting piece of %s", "information")
```

One function is available for each log level (see below).

The following fields are returned:
- The Git SHA, if passed at compile-time using the appropriate ldflag, like so `go build -ldflags "-X gitlab.com/adalonggroup/golog.GitSHA=$(git rev-parse HEAD)"`
- The message itself
- The log level (DEBUG | INFO | WARN | ERROR | FATAL). Using `golog.Fatalf` exits the program.
- The program name, passed in the same way as git Git SHA, or else the program basename.
- The time
- The environment, taking from the `ENV` env var, if available

## What else?
Nothing, that's pretty much it.
