package golog

type Severity int

const (
	DEFAULT Severity = iota * 100
	DEBUG
	INFO
	NOTICE
	WARNING
	ERROR
	CRITICAL
	ALERT
	EMERGENCY
)
