package golog

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"time"
)

type Log struct {
	GitSHA   string `json:"sha,omitempty"`
	Time     string `json:"timestamp"`
	Program  string `json:"program,omitempty"`
	Severity string `json:"severity"`
	Msg      string `json:"message"`
}

var (
	GitSHA             string
	Program            string
	logSeverity        Severity
	defaultLogSeverity = INFO
)

func init() {
	if sev, prs := os.LookupEnv("LOG_LEVEL"); prs {
		logSeverity = getLogSeverity(sev)
	} else {
		logSeverity = defaultLogSeverity
	}

	if GitSHA == "" {
		Warnf(`The variable GitSHA cannot be found. Please build the binary with the -ldflags "-X gitlab.com/adalonggroup/golog.GitSHA=$(git rev-parse HEAD)"`)
	}

	if Program == "" {
		Program = filepath.Base(os.Args[0])
	}
}

func Debugf(format string, args ...interface{}) {
	logWithSeverity(DEBUG, format, args...)
}

func Infof(format string, args ...interface{}) {
	logWithSeverity(INFO, format, args...)
}

func Warnf(format string, args ...interface{}) {
	logWithSeverity(WARNING, format, args...)
}

func Errorf(format string, args ...interface{}) {
	logWithSeverity(ERROR, format, args...)
}

func Fatalf(format string, args ...interface{}) {
	logWithSeverity(EMERGENCY, format, args...)
}

func logWithSeverity(severity Severity, format string, args ...interface{}) {
	if severity >= logSeverity {
		time, _ := time.Now().UTC().MarshalText()
		msg := fmt.Sprintf(format, args...)
		record := Log{
			GitSHA:   GitSHA,
			Time:     string(time),
			Program:  Program,
			Severity: severity.String(),
			Msg:      msg,
		}
		payload, _ := json.Marshal(record)
		fmt.Println(string(payload))
		if severity == EMERGENCY {
			os.Exit(1)
		}
	}
}

func getLogSeverity(sev string) Severity {
	switch sev {
	case "DEFAULT":
		return DEFAULT
	case "DEBUG":
		return DEBUG
	case "INFO":
		return INFO
	case "NOTICE":
		return NOTICE
	case "WARNING":
		return WARNING
	case "ERROR":
		return ERROR
	case "CRITICAL":
		return CRITICAL
	case "ALERT":
		return ALERT
	case "EMERGENCY":
		return EMERGENCY
	default:
		return DEFAULT
	}
}
